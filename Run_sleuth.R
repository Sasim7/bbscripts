#Pipeline to process kallisto,with annotation from biomart to be used with differential analysis using sleuth

suppressMessages(library("tximport"))
suppressMessages(library("biomaRt"))
suppressMessages(library("DESeq2"))
suppressMessages(library("ggplot2"))
suppressMessages(library("ggthemes"))
suppressMessages(library("reshape2"))
suppressMessages(library("factoextra"))
suppressMessages(library("FactoMineR"))
suppressMessages(library("dplyr"))
suppressMessages(library("sleuth"))
suppressMessages(library("rjson"))
suppressMessages(library("wasabi"))
suppressMessages(library("rtracklayer"))

options(mc.cores = 4L)

args = commandArgs(TRUE)

kallisto_res_folder_inp=args[9:length(args)]

kallisto_res_folder<-c()

for(i in 1:length(kallisto_res_folder_inp))
{
  
  kallisto_res_folder[i]<-dirname(kallisto_res_folder_inp[i])
  
}
threshold_variable=args[8]
gtf_file=args[7]
out_folder=args[6]

sampleinfofile = args[1]
ssetid = args[5]

q_thres = args[3]
p_thres = args[2]
fc_thres = args[4]

q_val_cut = as.numeric(q_thres)
p_val_cut = as.numeric(p_thres)
fc_val_cut = as.numeric(fc_thres)


##print(out_folder)

##print(sampleinfofile)

##print(kallisto_res_folder)

#path of the directory with the  kallisto quantification folders

#Read in information about the sample groups

exp_info<-read.delim(sampleinfofile,header = TRUE)

#print(exp_info)


#List of the files with the paths and type of file for each analysis

#print(kallisto_res_folder)

#kallisto_res_folder<-paste0(kallisto_res_folder,"/kallisto_quant/")
res_folder<-kallisto_res_folder[1]

file_check1<-list.files(path =res_folder ,pattern="quant.sf",full.names = T,all.files = T,recursive = T)
#print(file_check1)
file_check2<-list.files(path =res_folder ,pattern="abundance.tsv",full.names = T,all.files = T,recursive = T)
#print(file_check2)

if(length(file_check1)>0){
prepare_fish_for_sleuth(kallisto_res_folder)
  infofiles_kallisto<-file.path(kallisto_res_folder,"/aux_info/meta_info.json")
  #print(infofiles_kallisto)
  
  meta_info=fromJSON(file = infofiles_kallisto[i])
  run_info=data.frame(n_processed=meta_info$num_processed,n_mapped=meta_info$num_mapped)
  
  #print(run_info)
  
  for(i in 2:length(infofiles_kallisto)){
    tmp_meta_info=fromJSON(file = infofiles_kallisto[i])
    tmp_info=data.frame(n_processed=tmp_meta_info$num_processed,n_mapped=tmp_meta_info$num_mapped)
    run_info<-rbind(run_info,tmp_info)
  }
  
  run_info$sample<-basename(dirname(kallisto_res_folder))
  
  run_info$n_unmapped<-run_info$n_processed-(run_info$n_mapped)
  
  run_info_summary<-run_info[,c(3,2,4)]
  
  colnames(run_info_summary)<-c("sample","Assigned","Unassigned")
  
  
  runinfo_melted<-melt(run_info_summary,id.vars = "sample")
  colnames(runinfo_melted)<-c("Sample","variable","value")
  
}else if (length(file_check2)>0){


infofiles_kallisto<-file.path(kallisto_res_folder,"run_info.json")
#print(infofiles_kallisto)

run_info<-as.data.frame(fromJSON(file = infofiles_kallisto[1]))

#print(run_info)

for(i in 2:length(infofiles_kallisto)){
    tmp_info<-as.data.frame(fromJSON(file = infofiles_kallisto[i]))
    run_info<-rbind(run_info,tmp_info)
}

run_info$sample<-basename(dirname(kallisto_res_folder))

run_info$n_nonuniq<-run_info$n_pseudoaligned-(run_info$n_unique)
run_info$p_nonuniq<-(run_info$n_nonuniq*100)/run_info$n_processed

run_info_summary<-run_info[,c(12,3,4,5,13)]
run_info_summary$unaligned<-(run_info_summary$n_processed)-(run_info_summary$n_pseudoaligned)

colnames(run_info_summary)<-c("sample","TotalReads","Assigned","Uniquemappers","Multimappers","Unassigned")

for(j in 1:nrow(run_info_summary)){
run_info_summary[j,2:ncol(run_info_summary)]<-((run_info_summary[j,2:ncol(run_info_summary)])/run_info_summary[j,2])*100
}

runinfo_melted<-melt(run_info_summary[,c(1,4:6)],id.vars = "sample")
colnames(runinfo_melted)<-c("Sample","variable","value")
}

 
# #Create a transcript to gene annotations

 library("aws.dynamodb")
 suppressMessages(library("aws.ec2metadata"))
 crede=aws.signature::locate_credentials(verbose = F)
# 
 #Sys.setenv("AWS_ACCESS_KEY_ID" = "AKIAI26TH4KJI3TZ5OHA","AWS_SECRET_ACCESS_KEY" = "fThTXKDJ/1UocPDKWwbHTCM9f8cdQekvZz751Tux","AWS_DEFAULT_REGION" = "us-east-1") 
# 
 sampleset<-get_item("6173054311.1110008479.sampleset",list(hash_key=ssetid))
# 
 project_id<-sampleset$project_$S
# 
 account_id<-sampleset$account$S
# 
# 
 project<-get_item("6173054311.1110008479.Project",list(hash_key=paste0("6173054311.1110008479.",account_id,".Project.",project_id)))
# 
# 
 species<-project$organism_labels$S
 annotation_file<-read.table("/home/ec2-user/annotationData/Species_annotation.txt",header=T)
# 
 idx_species<-which(annotation_file$Organism== species)
# 
# t2g <- readRDS(paste0("/home/ec2-user/annotationData/",as.character(annotation_file[idx_species, ]$Biomartdataset), "_t2g.rds"))
get_t2g_fromgtf=function(gtf_file){
   gtf_filedata <- rtracklayer::import(gtf_file)
   gtf_df=as.data.frame(gtf_filedata)
   gene_name_exst=is.element("gene_name",colnames(gtf_df))
   if(gene_name_exst==TRUE){
     t2g=unique(gtf_df[,c("transcript_id","gene_id","gene_name")])
   }
   else{
     gtf_df$gene_name=gtf_df$gene_id
     t2g=unique(gtf_df[,c("transcript_id","gene_id","gene_name")])
   }
   t2g=na.omit(t2g)
   #rownames(t2g)<-t2g$gene_id
   t2g
 }
 
 #t2g <- readRDS(paste0("/home/ec2-user/annotationData/",as.character(annotation_file[idx_species, ]$Biomartdataset), "_t2g.rds"))

#id2gene<-unique(t2g[,c(2:3)])
t2g=get_t2g_fromgtf(gtf_file)
t2g$transcript_id=gsub("\\..*","",t2g$transcript_id)
t2g$gene_id=gsub("\\..*","",t2g$gene_id)
t2g <- dplyr::rename(t2g, target_id = transcript_id,ens_gene = gene_id, ext_gene = gene_name)

s2c<-exp_info
s2c=s2c[order(s2c$sample),]
s2c <- dplyr::mutate(s2c, path = kallisto_res_folder)

#so <- sleuth_prep(s2c_cut, ~condition, target_mapping = t2g)
# to run it in gene based analysis mode
#so <- sleuth_prep(s2c)
so <- sleuth_prep(s2c,target_mapping = t2g,aggregation_column = "ens_gene")
so <- sleuth_fit(so,~condition,'full')
save.image(paste0(out_folder,"/sleuth_prep.RData"))

n_conditions=length(levels(exp_info$condition))

so_list=list()

for(i in 1:(n_conditions-1)){
Ids<-paste0(unique(exp_info$condition)[1],unique(exp_info$condition)[i+1])

compare<-paste0("condition",levels(exp_info$condition)[i+1])

#perform differential expression analysis
so <- sleuth_wt(so, compare)
so_list[[i]]=so
 #so <- sleuth_fit(so, ~1, 'reduced')
# 
 #so <- sleuth_lrt(so, 'reduced', 'full')
# 

### write results 
results_table <- sleuth_results(so,compare,pval_aggregate=F)
results_table=na.omit(results_table)
results_table$target_id=gsub("\\|.*","",results_table$target_id)

results_table$target_id=gsub("\\..*","",results_table$target_id)
results_table=merge(results_table,t2g,by.x="target_id",by.y="target_id")
results_table=unique(results_table)
#Gene ontology and pathway analysis
volcano_df=na.omit(results_table)
sig_Genes<-volcano_df[volcano_df$pval < p_val_cut,]$ens_gene

if (threshold_variable =="pvalue"){
  sig_Genes<-volcano_df[volcano_df$pval < p_val_cut & abs(volcano_df$b) > fc_val_cut,]$ens_gene
} else if (threshold_variable =="qvalue"){
  sig_Genes<-volcano_df[volcano_df$qval < q_val_cut & abs(volcano_df$b) > fc_val_cut,]$ens_gene
}else {
  #print("Please choose either pvalue or qvalue as a threshold variable")
}
suppressMessages(library(gProfileR))
org_input<-as.character(annotation_file[idx_species, ]$Pathwaydataset)
genes_gprofile<-gprofiler(sig_Genes,  organism = org_input,significant=F)

genes_gprofile=genes_gprofile[order(genes_gprofile$p.value),]

genes_gprofile_bp=genes_gprofile[genes_gprofile$domain=="BP",]

write.csv(genes_gprofile_bp,paste0(out_folder,paste0("/GObp_results_",Ids,".csv")))

genes_KEGGgprofile<-gprofiler(sig_Genes,  organism = org_input,significant=F,src_filter='KEGG')

genes_KEGGgprofile=genes_KEGGgprofile[order(genes_KEGGgprofile$p.value),]

write.csv(genes_KEGGgprofile,paste0(out_folder,paste0("/KEGG_results_",Ids,".csv")))

#results_table <- sleuth_results(so, 'reduced:full','lrt',pval_aggregate=TRUE)
results_ordered <- unique(results_table[order(results_table$qval),])

# Writes tables for dif expressed genes
# write.table( subset(results_ordered, qval <= 0.001), file= paste('DE',Ids,'.qval_0.001.txt', sep = ''), sep="\t",row.names=F, quote=F)
write.csv(results_ordered,paste0(out_folder,paste0("/sleuth_res_",Ids,".csv")))
}
save.image(paste0(out_folder,"/sleuth_res.RData"))
