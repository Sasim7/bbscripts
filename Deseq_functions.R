ddsTxi_obj <- DESeqDataSetFromMatrix(cdata,colData = exp_info,design = ~ condition)
tx_kallisto <- tximport(files_kallisto, type="kallisto",tx2gene = t2g[,1:2],ignoreTxVersion=TRUE)
ddsTxi_obj <- DESeqDataSetFromTximport(tx_kallisto,
                                       colData = exp_info,
                                       design = ~ condition)

ddsTxi_obj <- DESeq(ddsTxi_obj)


group_pairs<-combn(unique(exp_info$condition),2,simplify = T)

runDea=function(group_pair){
 deseqResults = data.frame(results(deseqobj,contrast = c("condition",as.character(group_pairs[,group_pair]))))
 fname = paste0("DESeq2_res", as.character(group_pairs[,group_pair])[1],"Vs",as.character(group_pairs[,group_pair])[2],".csv")
 write.csv(deseqResults, fname)
 
 deseqResults_filt<-deseqResults[!is.na(deseqResults$padj),]
 rownames(deseqResults_filt)<-deseqResults_filt$Row.names
 sig_Genes<-rownames(deseqResults_filt[deseqResults_filt$pvalue < p_val_cut,])
 sig_Genes
}

species<-project$organism_labels$S
annotation_file<-read.table("/home/ec2-user/annotationData/Species_annotation.txt",header=T)
idx_species<-which(annotation_file$Organism== species)
org_input<-as.character(annotation_file[idx_species, ]$Pathwaydataset)

runGO=function(sigGenes,lfcCut){
  
  genes_gprofile<-gprofiler(id2gene[sig_Genes,]$external_gene_name,
                            organism = org_input,significant=F)
  genes_gprofile=genes_gprofile[order(genes_gprofile$p.value),]
  genes_gprofile_bp=genes_gprofile[genes_gprofile$domain=="BP",]
 
  genes_kegggprofile<-gprofiler(id2gene[sig_Genes,]$external_gene_name,  organism = org_input,significant=F,src_filter='KEGG')
  genes_kegggprofile=genes_kegggprofile[order(genes_kegggprofile$p.value),]
  
  write.csv(genes_gprofile_bp,paste0(out_folder,"/GObp_results_",comp,".csv"))
  write.csv(genes_kegggprofile,paste0(out_folder,"/KEGGpathway_results_",comp,".csv"))
  
  genes_gprofile_bp_list[[comp]]=genes_gprofile_bp
  genes_kegggprofile_list[[comp]]=genes_kegggprofile
  list(genes_gprofile_bp_list,genes_kegggprofile_list)
  
}

lapply(X = list(1:ncol(group_pair)),FUN = runDea(x))