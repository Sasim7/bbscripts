---
title: "Transcriptomic analysis with DESeq2"
params:
  df:
    value: x
  of:
    value: x
  pipid:
    value: x
  ssetid:
    value: x
  refid:
    value: x
output:
  html_document:
    df_print: paged
    fig_height: 10
    fig_width:  10
    includes:
    number_sections: no
    theme: united
    toc: yes
    toc_depth: 2
    toc_float: yes
  pdf_document:
    toc: yes
    toc_depth: '2'
---

<style type="text/css">

body{ /* Normal  */
      font-size: 16px;
      font-family: "Times New Roman", Times, serif;
  }

h1.title {
  font-size: 26px;
  font-family: "Times New Roman", Times, serif;
}
h1 { /* Header 1 */
  font-size: 22px;
  font-family: "Times New Roman", Times, serif;
}
h2 { /* Header 2 */
    font-size: 22px;
    font-family: "Times New Roman", Times, serif;
    color:#145A32;
    font-weight: bold;
}
h3 { /* Header 3 */
  font-size: 22px;
  font-family: "Times New Roman", Times, serif;
}

</style>


## Analysis Overview {.tabset .tabset-fade}

### Project Details

```{r, echo = FALSE, out.width="50%",message=FALSE,fig.align='center',comment=NA,warning=FALSE}


```

```{r, echo = FALSE, out.width="50%",message=FALSE,fig.align='center',comment=NA,warning=FALSE}



```

**Project** : `sample`     

**Organism** : `sample`     

**Details** : `sample`  

**Description** : `sample`   

**Date** : `sample`   

**Sample Type** : `sample`   

**Experiment Type** : `sample`   

### Sample Summary

**Sampleset Name** : `sample`  

**Number of Samples** : `10`

**Sample Names** : `sample`    

```{r, echo = FALSE,message=FALSE,fig.align='center',comment=NA,warning=FALSE,out.width="50%"}


```



### Pipeline Details


```{r, echo = FALSE, out.width="50%",message=FALSE,fig.align='center',comment=NA,warning=FALSE}


```

**PipelineName** : `sample`

**Description** : `sample`

**Details** : `sample`  

### Tools
**Tools used in the pipeline**

```{r, echo = FALSE, out.width="50%",message=FALSE,fig.align='center',comment=NA,warning=FALSE}



```

## Sample Quality & Alignment Metrics {.tabset .tabset-fade}

### Sample Quality


```{r, echo = FALSE, out.width="50%",message=FALSE,fig.align='center',comment=NA,warning=FALSE}



```

**Quality assessment of sample read files by FastQC**

```{r, echo =FALSE,message = FALSE,warning = FALSE,comment = NA}

```   




### Mapping Statistics

**Alignment statistics**

```{r, echo = FALSE,message=FALSE,fig.align='center',comment=NA,warning=FALSE,out.width="50%"}


```



### Normalization Summary

**Expression values after normalization**

Normalization was done using DESeq2-regularized logarithm or rlog, which normalizes variance of each gene expression across all samples so that both low expressed as well as highly expressed genes have similar technical variance.


```{r, echo = FALSE,fig.align="center",comment=NA,fig.show='hold',warning=FALSE,out.width="50%"}


```

Read counts of each gene in all samples were normalized for library size and mRNA composition bias.


## Differential Analysis {.tabset .tabset-fade}

```{r, echo=FALSE, out.width="50%",fig.align='center',comment=NA,warning=FALSE}


```


### Gene Table

Top 200 differentially expressed genes are shown below and the complete list can be downloaded from Reports -> File List section


```{r, echo=FALSE,results='asis', out.width="50%",fig.align='center',comment=NA,warning=FALSE}


```

Differential gene expression is calculated using DESeq2 with default parameters.DESeq2  estimates the magnitude of differential expression between experimental conditions based on read counts in the sample replicates.First, a normalization step is carried out to mitigate the effect of differences in sequencing depth and variability. It then calculates the significance of the estimated expression difference across different experimental conditions, provided as FDR (false discovery rate) or adjusted P-value. Biological significance is provided as magnitude of change (or fold change). It is calculated as log2FoldChange and assumes that up and down changes are symmetrical.Estimation, however, assumes that different genes possess similar variance in the same experimental condition.

**baseMean** : The ratio of the average normalized counts in two experimental conditions (control and treatment), averaged over all replicates in each condition.

**log2FoldChange** : Logarithmic fold change in the expression of a gene between different experimental conditions. It is described as fold or times increase in the change. In other words a logfoldchange of -1 indicates that gene is down regulated in its' expression by 2 times compared to control and a l2fc of 1 indicates gene is upregulated by 2 times compared to control.

**p-value** :  Probability that the observed fold change is seen due to sampling or experimental error.

**P-adj** : False discovery (Benjamini-Hochberg corrected) adjusted p-value. Inferring expression change from normalized count values involves multiple statistical analysis resulting into type I error i.e. determining fold change when change is due to technical variance. The rate of type I error is called as FDR (False Discovery Rate). This can be corrected using Benjamini-Hochberg correction. This improved p-value is described as P-adj.

Detailed information about DESeq2 can be found in the manual - https://bioc.ism.ac.jp/packages/2.14/bioc/vignettes/DESeq2/inst/doc/beginner.pdf

### Heatmap


```{r, echo=FALSE,fig.align='center',comment=NA,warning=FALSE,message=FALSE}


```

HeatMaps are used in RNA-seq for graphical representation of the level of expression of several genes across multiple samples under different conditions.Colour intensity and size of the box indicate the variation in the data value.Association between the experimental conditions (or samples) is shown across columns and the variation in expression levels is shown across rows. Data points are grouped together in rows and columns using hierarchical clustering method.Wherein, average pairwise distance is used as a measure for clustering. Rows (gene expression across all samples), i.e. expression profile of a gene in different samples, and columns (gene expression across all genes), i.e. expression of different genes in one sample are grouped on basis of similar profiles.A dendrogram alongside the heatmap shows the similarity and the order in which clusters are formed. 
Genes that are downregulated are indicated in blue colour and the upregulated genes are shown in red colour.


### PCA


```{r,echo =FALSE, out.width="50%",comment=NA,warning=FALSE,fig.align="center"}



```

Principal component analysis (PCA) is a technique for exploring variability in the data such as expression (RNA-seq) data and for dimensionality reduction.PCA computes a linear transformation of the data into an orthogonal coordinate system called principal components such that the principal components are uncorrelated and at the same time, the variability in the data is preserved as much. PCA is done by performing Eigenvalue decomposition of the covariance matrix generated from the sample data (such as expression data).Variance of the read count from the expression mean projected on the line of best fit among all the data points is the first principal component.The second principle component is orthogonal to the first.Projecting the data to the first few principal components reduces the data dimension while retaining as much data variability. This is especially helpful in data visualization and clustering. 

### Volcano Plot

```{r, echo=FALSE,message=FALSE,comment=NA,fig.show='hold',warning=FALSE,out.width="50%",fig.align="center"}


```

Volcano plot is used to view significantly differentially expressed genes.It is a scatter plot between log fold change of expression among different biological conditions and the significance of the change determined from p-value.Volcano plots enable visual inspection of expression change across all the genes. Each dot in the plot represent a gene and it's value on x-axis (log2foldchange) and y-axis (log of p-value) is determined after normalizing across replicates in two different conditions.Genes (or dots) of maximum interest are the dots in the top right quadrant ( above the dotted line in the graph ). These dots possess differential expression with maximum confidence (high y-axis value). Their x-axis value indicate either high negative log-fold change i.e. down regulation (red colour dots) or high positive log-fold change i.e. up regulation (blue colour dots).

     
## Gene Set Enrichment Analysis {.tabset .tabset-fade}

### Gene Ontology

Gene ontology enrichment analysis of differentially expressed genes. (Complete list can be downloaded from Reports -> File List section)

```{r,echo=FALSE,message=FALSE,fig.align='center',comment=NA,fig.show='hold',warning=FALSE,out.width="50%",results='asis'}

```

GO term enrichment is carried out for the genes that are significantly over/under-expressed due to treatment. This enrichment indicates the main functions/processes affected by the treatment.
Gene ontology refers to hierarchical set of terms grouped in three categories; molecular function (main function of a gene); biological process (main pathway/process in which gene is involved) and cellular location (location in the cell).

**Term** : The name of the GO term to which the differentially expressed genes are annotated.

**p-value** : Probability that the reported GO term is reported by chance.

**Overlap** : Number of differentially expressed genes which are part of the GO term.



### Pathways

KEGG pathways enrichment analysis of differentially expressed genes.(Complete list can be downloaded from Reports -> File List section)

KEGG combine information from different databases. Pathway enrichment analysis on deferentially expressed genes highlights functional pathways, disease networks or cellular systems that are affected by the treatment/experimental condition

**Term** : The name of the KEGG pathway to which the differentially expressed genes belong to.

**p-value** : Probability that the reported KEGG pathway is enriched by chance.

**Overlap** : Number of differentially expressed genes which are part of the KEGG pathway.




```{r,echo=FALSE,message=FALSE,fig.align='center',comment=NA,fig.show='hold',warning=FALSE,out.width="50%",results='asis'}


```

